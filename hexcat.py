#!/usr/bin/env python3

import sys
import binascii

fileName = sys.argv[1]

with open(fileName, "rb") as f:
    i = 0
    while True:        
        byte = f.read(1)
        if byte == b'':
            break
        if i == 0:
            pass
        elif (i % 32) == 0:
            print()
        elif (i % 4) == 0:
            print(' ', end='')
        print(binascii.hexlify(byte).decode("utf-8"), end=' ')
        i += 1
